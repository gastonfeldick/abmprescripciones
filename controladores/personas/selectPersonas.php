<?php
require('../../modelos/personas.php');

$personas= persona::pacientes();  //retorna todos los pacientes

$jason=array();
while($persona=$personas->fetch_object()){
    $jason[]=array(
        'id'=>$persona->id,
        'nombre'=>$persona->nombre,
        'apellido'=>$persona->apellido,
        'dni' => $persona->dni,
        'fecha_nacimiento'=>$persona->fecha_nacimiento,
    );
}

$string=json_encode($jason);
echo($string);


?>