<?php
require('../../modelos/medicamentos.php');

$medicamentos= medicamento::selectMediamentos();  //retorna todos los medicamentos

$jason=array();
while($medicamento=$medicamentos->fetch_object()){
    $jason[]=array(
        'id'=>$medicamento->id,
        'nombre'=>$medicamento->nombre_comercial,
    );
}

$string=json_encode($jason);
echo($string);
?>