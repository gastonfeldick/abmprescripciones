<?php
    require_once("../../modelos/prescripciones.php");
    $id=$_GET["id"];
    $prescripciones=prescripcion::selectPrescripcion($id);

    $jason=array();
    while($prescripcion=$prescripciones->fetch_object()){
        $jason[]=array(
            'id'=>$prescripcion->id,
            'observaciones'=>$prescripcion->observaciones,
            'medicamento_id'=>$prescripcion->medicamento_id,
            'persona_id'=>$prescripcion->persona_id, 	 
        );
    }

    $string=json_encode($jason);
    echo($string);

?>