<?php
require_once("../../modelos/prescripciones.php");
    $medicamento=$_POST["medicamento"];
    $prescripciones=prescripcion::prescripcionMedicamento($medicamento);

    $jason=array();
    while($prescripcion=$prescripciones->fetch_object()){
        $jason[]=array(
            'id'=>$prescripcion->id,
            'nombre'=>$prescripcion->nombre,
            'apellido'=>$prescripcion->apellido,
            'dni'=>$prescripcion->dni,
            'observaciones'=>$prescripcion->observaciones,
            'medicamento'=>$prescripcion->nombre_comercial,
            'fecha'=>$prescripcion->fecha, 	
        );
    }

    $string=json_encode($jason);
    echo($string);

?>