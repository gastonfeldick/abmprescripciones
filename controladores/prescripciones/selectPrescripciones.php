<?php
    require_once("../../modelos/personas.php");
    require_once("../../modelos/medicamentos.php");
    require_once("../../modelos/prescripciones.php");
    
    $prescripciones=prescripcion::selectPrescripciones();

    $jason=array();
    while($prescripcion=$prescripciones->fetch_object()){
        $jason[]=array(
            'id'=>$prescripcion->id,
            'nombre'=>$prescripcion->nombre,
            'apellido'=>$prescripcion->apellido,
            'dni'=>$prescripcion->dni,
            'observaciones'=>$prescripcion->observaciones,
            'medicamento'=>$prescripcion->nombre_comercial,
            'fecha'=>$prescripcion->fecha, 	
        );
    }

    $string=json_encode($jason);
    echo($string);

?>