<?php
require_once('conexion.php');
class persona extends conexion{

    protected $id;
    protected $nombre;
    protected $apellido;
    protected $dni;
    protected $fechaNacimiento;

    public function __construct($id='',$nombre='',$apellido='',$dni='',$fechaNacimiento=''){
        $this->id=$id;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->dni=$dni;
        $this->fechaNacimiento=$fechaNacimiento;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function setNombre($nombre){
        $this->nombre=$nombre;
    }
    public function setApellido($apellido){
        $this->apellido=$apellido;
    }
    public function setDni($dni){
        $this->dni=$dni;
    }
    public function setFechaNacimiento($fecha){
        $this->fechaNacimiento=$fecha;
    }

    public function getId(){
        return $this->id;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function getApellido(){
        return $this->apellido;
    }
    public function getDni(){
        return $this->dni;
    }
    public function getFecha(){
        return $this->fechaNacimiento;
    }

    public function getAll(){
        $array = array(
            "id" => $this->id,
            "nombre" => $this->nombre,
            "apellido" => $this->apellido,
            "dni" => $this->dni,
            "fechaNacimiento" => $this->fechaNacimiento,
        );

        return $array;
    }

    public function setAll($id,$nombre,$apellido,$dni,$fecha){
            $this->id=$id;
            $this->nombre=$nombre;
            $this->apellido=$apellido;
            $this->dni=$dni;
            $this->fechaNacimiento=$fecha;
    }

    public static function Pacientes(){

        $db= new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("SELECT * FROM personas");
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
        
    }
    
    public static function selectPersona($ide){
        
        $db= new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("SELECT * FROM personas WHERE id=?");
        $stmt->bind_param('s', $ide);
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;


    }
    
    public function insertarPersona(){
        $db=new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("INSERT INTO `personas` ( `nombre`, `apellido`, `dni`, `fecha_nacimiento`) VALUES (?, ?, ?, ?) ");
        $stmt->bind_param('ssss',$this->nombre,$this->apellido,$this->dni,$this->fechaNacimiento);
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
    }

    public function updatePersona(){
        $db=new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare(" UPDATE `personas` SET `nombre` = ?, `apellido` = ?, `dni` = ?, `fecha_nacimiento` = ? WHERE `personas`.`id` = ?");
        $stmt->bind_param('sssss',$this->nombre,$this->apellido,$this->dni,$this->fechaNacimiento,$this->id);
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
    }

    public function eliminarPersona(){
        $db=new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("DELETE FROM `personas` WHERE `personas`.`id` = ? ");
        $stmt->bind_param('s',$this->id);
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
    }

    public static function selectPersonaDni($dni){
        
        $db= new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("SELECT * FROM personas WHERE dni=?");
        $stmt->bind_param('s', $dni);
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;


    }



}
?>