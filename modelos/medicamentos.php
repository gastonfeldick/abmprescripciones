<?php
require_once("conexion.php");
class medicamento extends conexion{
    protected $id, $nombreComercial;

    public function __construct($id=0,$nombre=""){
        $this->id=$id;
        $this->nombreComercial=$nombre;
    }

    public function setId($id){
        $this->id=$id;
    }
    public function setNombreComercial($nombre){
        $this->nombreComercial=$nombre;
    }
    public function getId(){
        return $this->id;
    }
    public function getNombreComercial(){
        return $this->nombreComercial;
    }

    public function insertarMedicamento(){
        $db=new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("INSERT INTO `medicamentos` (`nombre_comercial`) VALUES (UPPER(?))");
        $stmt->bind_param('s',$this->nombreComercial);
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
    }

    public static function existeMedicamento($medicamento){
        $db=new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("SELECT * FROM medicamentos WHERE nombre_comercial=UPPER(?)");
        $stmt->bind_param('s',$medicamento );
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
    }
    public static function selectMediamentos(){
        $db=new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("SELECT * FROM medicamentos");
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
    }
    public static function selectMediamento($id){
        $db=new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("SELECT * FROM medicamentos WHERE id=?");
        $stmt->bind_param('i',$id );
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
    }

    public function updateMedicamento(){
        $db=new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("UPDATE medicamentos SET nombre_comercial=upper('$this->nombreComercial') WHERE id=?");
        $stmt->bind_param('i',$this->id );
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
    }

    public function eliminarMedicamento(){
        $db=new conexion();
        $db->connect();
        $con=$db->conexion;
        $stmt = $con->prepare("DELETE FROM medicamentos WHERE id =? ");
        $stmt->bind_param('s',$this->id);
        $stmt->execute();
        $result=$stmt->get_result();
        return $result;
    }

}

?>