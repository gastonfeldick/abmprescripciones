<?php
    require_once ("conexion.php");
    class prescripcion extends conexion{
        protected $id,$observaciones,$idPersona,$idMedicamento;

        public function __construct($id=0,$observaciones="",$idPersona=0,$idMedicamento=0){
            $this->id=$id;
            $this->observaciones=$observaciones;
            $this->idPersona=$idPersona;
            $this->idMedicamento=$idMedicamento;
        }

        public function setId($id){
            $this->id=$id;
        }
        public function setObservaciones($observaciones){
            $this->observaciones=$observaciones;
        }
        public function setIdPersona($idPersona){
            $this->idPersona=$idPersona;
        }
        public function setIdMedicamento($idMedicamento){
            $this->idMedicamento=$idMedicamento;
        }

        public function insertarPrescripcion(){
            $db=new conexion();
            $db->connect();
            $con=$db->conexion;
            $stmt = $con->prepare("INSERT INTO `persona_medicamento` ( `observaciones`, `persona_id`, `medicamento_id`) VALUES (?,?,?) ");
            $stmt->bind_param('sii',$this->observaciones,$this->idPersona,$this->idMedicamento);
            $stmt->execute();
            $result=$stmt->get_result();
            //$stmt->close();
            return $result;
        }

        public static function selectPrescripciones(){
            $db=new conexion();
            $db->connect();
            $con=$db->conexion;
            $stmt = $con->prepare("SELECT consulta.id,consulta.observaciones,date_format(consulta.created_at,'%d/%m/%Y') as fecha,personas.nombre,personas.apellido,personas.dni,medicamentos.nombre_comercial FROM persona_medicamento as consulta, personas, medicamentos WHERE consulta.persona_id=personas.id AND consulta.medicamento_id=medicamentos.id ORDER BY consulta.id ASC;");
            $stmt->execute();
            $result=$stmt->get_result();
            //$stmt->close();
            return $result;
        }

        public static function selectPrescripcion($id){
            $db=new conexion();
            $db->connect();
            $con=$db->conexion;
            $stmt = $con->prepare("SELECT consulta.id,consulta.observaciones,consulta.persona_id, consulta.medicamento_id FROM persona_medicamento as consulta, personas, medicamentos WHERE consulta.persona_id=personas.id AND consulta.medicamento_id=medicamentos.id AND consulta.id=?;");
            $stmt->bind_param('i',$id);
            $stmt->execute();
            $result=$stmt->get_result();
            //$stmt->close();
            return $result;
        }
        
        public function updatePrescripcion(){
            $db=new conexion();
            $db->connect();
            $con=$db->conexion;
            $stmt = $con->prepare("UPDATE `persona_medicamento` SET `observaciones` = ?, `persona_id` = ?, `medicamento_id` = ? WHERE `persona_medicamento`.`id` = ? ");
            $stmt->bind_param('siii',$this->observaciones,$this->idPersona,$this->idMedicamento,$this->id);
            $stmt->execute();
            $result=$stmt->get_result();
            //$stmt->close();
            return $result;
        }

        public function eliminarPrescripcion(){
            $db=new conexion();
            $db->connect();
            $con=$db->conexion;
            $stmt = $con->prepare("DELETE FROM `persona_medicamento` WHERE `persona_medicamento`.`id` = ?");
            $stmt->bind_param('i',$this->id);
            $stmt->execute();
            $result=$stmt->get_result();
            //$stmt->close();
            return $result;
        }

        public static function prescripcionDni($dni){
            $db=new conexion();
            $db->connect();
            $con=$db->conexion;
            $stmt = $con->prepare("SELECT consulta.id,consulta.observaciones,date_format(consulta.created_at,'%d/%m/%Y') as fecha,personas.nombre,personas.apellido,personas.dni,medicamentos.nombre_comercial FROM persona_medicamento as consulta, personas, medicamentos WHERE consulta.persona_id=personas.id AND consulta.medicamento_id=medicamentos.id AND personas.dni=? ORDER BY consulta.id ASC;");
            $stmt->bind_param('s',$dni);
            $stmt->execute();
            $result=$stmt->get_result();
            $stmt->close();
            return $result;
        }
        
        public static function prescripcionMedicamento($medicamento){
            $db=new conexion();
            $db->connect();
            $con=$db->conexion;
            $medicamento.="%";
            $stmt = $con->prepare("SELECT consulta.id,consulta.observaciones,date_format(consulta.created_at,'%d/%m/%Y') as fecha,personas.nombre,personas.apellido,personas.dni,medicamentos.nombre_comercial FROM persona_medicamento as consulta, personas, medicamentos WHERE consulta.persona_id=personas.id AND consulta.medicamento_id=medicamentos.id AND medicamentos.nombre_comercial LIKE UPPER(?) ORDER BY consulta.id ASC;");
            $stmt->bind_param('s',$medicamento);
            $stmt->execute();
            $result=$stmt->get_result();
            $stmt->close();
            return $result;
        }
    }

?>