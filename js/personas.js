$(document).ready(function(){
    pacientes();

    $('#añadir').click(function(){
        $('#formularioPersonas').show(500);
        $('#formularioPersonas').trigger('reset');
        $('#tabla').hide(3000);
    });
    $('#cancelar').click(function(){  
        $('#formularioPersonas').trigger('reset');
        $('#formularioPersonas').hide(1000);
        $('#tabla').show(2000);
    });

    $("#formularioPersonas").submit(function(e){

        $.ajax({
            url: '../controladores/personas/insertPersona.php',
            type: 'post',
            data: $('#formularioPersonas').serialize(),
            success: function(respuesta){
                
                if(respuesta=="insertado"){
                    $('#guardarPersona').show();
                    $('#guardarPersona').delay(2000);
                    $('#guardarPersona').hide(5000);
                    $('#formularioPersonas').delay(2000);
                    $('#formularioPersonas').hide(500);
                    $('#tabla').show(3000);
                    
                    pacientes();
                }else{
                    $('#errorPersona').show();
                    $('#errorPersona').delay(2000);
                    $('#errorPersona').hide(5000);
                }
                console.log(respuesta);
            }
        });


        e.preventDefault();
    });

    function pacientes(){
        $.ajax({
            url: '../controladores/personas/selectPersonas.php',
            type: 'get',
            success: function(respuesta){
                console.log(respuesta);
                let res=JSON.parse(respuesta);
                console.log(res);
                
                let template="";

                res.forEach(r =>{
                  template+=`
                    <tr class="" id=${r.id}>
                        <td scope="row">${r.id}</td>
                        <td>${r.nombre}</td>
                        <td>${r.apellido}</td>
                        <td>${r.dni}</td>
                        <td>${r.fecha_nacimiento}</td>
                        <td>
                            <button class="btn btn-primary editar">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                </svg>
                            </button >
                            <button class="btn btn-danger eliminar">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                                </svg>
                            </button>
                        </td>
                    </tr>
                  `;
                });
              $('#bodyPersonas').html(template);

            }
        });
    }

    $(document).on('click','.editar',function(e){
        let variable=$(this)[0].parentElement.parentElement;
        const id=$(variable).attr('id');

        $.ajax({

            url:"../controladores/personas/selectPersona.php",
            type:"get",
            data:{id},
            success:function(respuesta){
                console.log(respuesta);
                
                let res= JSON.parse(respuesta);
                res.forEach(r=>{
                    $("#update").show();
                    $("#tabla").hide(1000);
                    $("#updateId").val(r.id);
                    $("#updateNombre").val(r.nombre);
                    $("#updateApellido").val(r.apellido);
                    $("#updateDni").val(r.dni);
                    $("#updateFecha").val(r.fecha_nacimiento);
                });
            }

        });

        e.preventDefault();
    });

    $('#update').submit(function(e){
        $.ajax({
            url: '../controladores/personas/updatePersona.php',
            type: 'post',
            data: $('#update').serialize(),
            success: function(respuesta){
                $("#update").hide();
                $("#tabla").show(1000);
                pacientes();

                console.log(respuesta);
            }
        });
        e.preventDefault();
    });

    $(document).on('click','.eliminar',function(e){
        let variable=$(this)[0].parentElement.parentElement;
        const id=$(variable).attr('id');


        if (confirm("¿Desea eliminar a la persona?")){
            $.ajax({
                url:"../controladores/personas/eliminarPersona.php",
                type:"post",
                data:{id},
                success:function(respuesta){
                    console.log(respuesta);
                    pacientes();
                }
            })
        }



        e.preventDefault();
    });

    $('#menu').click(function(){
        window.location.href="../index.php";
    });






});