$(document).ready(function(){
    tabla();

    $('#añadir').click(function(){
        $('#principal').show(2000);
        $('#formularioMedicamento').trigger('reset');
        $('#tabla').hide(3000);
    });

    $('#cancelar').click(function(){  
        $('#formularioMedicamento').trigger('reset');
        $('#principal').hide(1000);
        $('#tabla').show(2000);
    });

    $('#formularioMedicamento').submit(function(e){
        let nombre= $('#nombre').val();
        $.ajax({
            url: "../controladores/medicamentos/insertarMedicamento.php",
            type: "post",
            data: {nombre},
            success: function(respuesta){
                console.log(respuesta);
                if(respuesta=="insertado"){
                    $('#tabla').show(3000);
                    tabla();
                    $('#principal').hide(3000);
                }
                if(respuesta=="ya existe"){
                    $('#errorMedicamento').show()
                    $('#errorMedicamento').delay(1000)
                    $('#errorMedicamento').hide(3000)
                }
            }
        });
        e.preventDefault();
    });

    function tabla(){
        $.ajax({
            url: '../controladores/medicamentos/selectMedicamentos.php',
            type: 'get',
            success: function(respuesta){
                console.log(respuesta);
                
                let res=JSON.parse(respuesta);
                console.log(res);
                
                let template="";

                res.forEach(r =>{
                  template+=`
                    <tr id=${r.id}>
                        <td scope="row">${r.id}</td>
                        <td>${r.nombre}</td>
                        <td>
                            <button class="btn btn-primary editar">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                </svg>
                            </button >
                            <button class="btn btn-danger eliminar">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                                </svg>
                            </button>
                        </td>
                    </tr>
                  `;
                });
              $('#bodyMedicamentos').html(template);

            }
        });
    }
    
    $(document).on('click','.editar',function(e){
        let variable=$(this)[0].parentElement.parentElement;
        const id=$(variable).attr('id');

        $.ajax({

            url:"../controladores/medicamentos/selectMedicamento.php",
            type:"get",
            data:{id},
            success:function(respuesta){
                console.log(respuesta);
                
                let res= JSON.parse(respuesta);
                res.forEach(r=>{
                    $('#tabla').hide(3000)
                    $("#update").show(2000);
                    
                    $("#updateId").val(r.id);
                    $("#updateNombre").val(r.nombre);
                });
            }

        });

        e.preventDefault();
    });

    $('#update').submit(function(e){
        $.ajax({
            url: '../controladores/medicamentos/updateMedicamento.php',
            type: 'post',
            data: $('#update').serialize(),
            success: function(respuesta){
                $("#update").hide(2000);
                $('#tabla').show(3000)
                tabla();
            }
        });
        e.preventDefault();
    });

    $(document).on('click','.eliminar',function(e){
        let variable=$(this)[0].parentElement.parentElement;
        const id=$(variable).attr('id');


        if (confirm("¿Desea eliminar el medicamento?")){
            $.ajax({
                url:"../controladores/medicamentos/eliminarMedicamento.php",
                type:"post",
                data:{id},
                success:function(respuesta){
                    console.log(respuesta);
                    tabla();
                }
            })
        }
        e.preventDefault();
    });

    $('#menu').click(function(){
        window.location.href="../index.php";
    });

    

});