<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prescripcion</title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="estilos/prescripciones.css" rel="stylesheet">
    <link rel="icon" href="https://www.integrandosalud.com/src-v2/public/bootstrap/img/favicon/favicon.ico" type="image/x-icon">
</head>
<body>
    <div id="nav">
        <button class="btn btn-success" id="menu"> MENU</button>
        <h1 id="titulo">PRESCRICPCIONES</h1>       
    </div>

    <form action="" id="prescripcion" style="display:none !important;">
        <div class="container">
        
            <div class="card">
                <div class="card-header">
                    Nueva prescripción
                </div>
                <div class="card-body">
                    <div class="row mt-1">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <select id="nombre" name="nombre" class="form-select" aria-label="Default select example" required>
    
                            </select>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="form-group">
                            <label for="medicamentos">Medicamento</label>
                            <select id="medicamentos" name="medicamentos" class="form-select" aria-label="Default select example" required>
                                
                            </select>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="form-group">
                            <label for="observaciones">Obsercavaciones</label>
                            <input id="observaciones" name="observaciones"class="form-control" type="text" required>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    <button type="submit" class="btn btn-success">Guardar</button>
                    <button id="cancelar"type="button" class="btn btn-danger">Cancelar</button>
                </div>
            </div>
        </div>
    </form>

    <div class="container mt-5 text-center" id="tabla">

        <div class="filtro">
            <div class="col col-4" id="filtroDni">
                <div class="input-group mb-3">
                    <input type="text"
                        class="form-control col-4" name="searchDni" id="searchDni" placeholder="Dni">
                    <button class="btn btn-success col-4" id="btnDni">Buscar</button>
                </div>
            </div>
            <div class="col col-4" id="filtoNombre">
                <div class="input-group mb-3">
                    <input type="text"
                        class="form-control col-4" name="searchMedicamento" id="searchMedicamento" placeholder="Medicamento">
                    <button class="btn btn-success col-4" id="btnMedicamento">Buscar</button>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-primary table-hover">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">NOMBRE Y APELLIDO</th>
                        <th scope="col">DNI</th>
                        <th scope="col">MEDICAMENTO</th>
                        <th scope="col">OBSERVACIONES</th>
                        <th scope="col">FECHA</th>
                        <th scope="col"> <button id="añadir" class="btn btn-success"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill-add" viewBox="0 0 16 16">
                            <path d="M12.5 16a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7Zm.5-5v1h1a.5.5 0 0 1 0 1h-1v1a.5.5 0 0 1-1 0v-1h-1a.5.5 0 0 1 0-1h1v-1a.5.5 0 0 1 1 0Zm-2-6a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
                            <path d="M2 13c0 1 1 1 1 1h5.256A4.493 4.493 0 0 1 8 12.5a4.49 4.49 0 0 1 1.544-3.393C9.077 9.038 8.564 9 8 9c-5 0-6 3-6 4Z"/>
                            </svg>  
                            Añadir</button>
                        </th>
                    </tr>
                </thead>
                <tbody id="bodyPrescripciones">

                </tbody>
            </table>
        </div>
        
    </div>
    <form action="" id="updatePrescripcion" style="display:none !important;">
        <div class="container" >
        
            <div class="card">
                <div class="card-header">
                    Actualizar Prescripcion 
                </div>
                <div class="card-body">
                    <input type="hidden" name="updateId" id="updateId">
                    <div class="row mt-1">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <select id="nombreUpdate" name="nombreUpdate" class="form-select" aria-label="Default select example" required>
    
                            </select>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="form-group">
                            <label for="medicamentos">Medicamento</label>
                            <select id="medicamentosUpdate" name="medicamentosUpdate" class="form-select" aria-label="Default select example" required>
                                
                            </select>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="form-group">
                            <label for="observaciones">Obsercavaciones</label>
                            <input id="observacionesUpdate" name="observacionesUpdate" class="form-control" type="text" required>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    <button type="submit" class="btn btn-success">Guardar</button>
                    <button id="cancelarUpdate"type="button" class="btn btn-danger">Cancelar</button>
                </div>
            </div>
        
        </div>
    </form>
    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>
    <script src="../js/jquery-3.6.3.min.js"></script>
    <script src="../js/prescripciones.js"></script>
</body>
</html>