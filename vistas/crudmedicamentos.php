<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Medicamentos</title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="estilos/medicamentos.css" rel="stylesheet">
    <link rel="icon" href="https://www.integrandosalud.com/src-v2/public/bootstrap/img/favicon/favicon.ico" type="image/x-icon">
</head>
<body>
    <div id="nav">
        <button class="btn btn-success" id="menu"> MENU</button>
        <h1 id="titulo">MEDICAMENTOS</h1>       
    </div>

    
    <div class="container" id="principal" style="display:none !important;">
        <div class="col-6" id="form">
            <form id="formularioMedicamento" >
                <div class="card">
                    <div class="card-header">
                        Nuevo Medicamento
                    </div>
                    <div class="card-body">
                        <div id="guardarMedicamento" class="alert alert-success text-center" style="display:none !important;">Guardado</div>
                        <div id="errorMedicamento" class="alert alert-danger text-center" style="display:none !important;">El medicamento ya existe</div>
                        <div class="row mt-1">
                            <div class="form-group">
                                <label for="nombre">Nombre Comercial</label>
                                <input id="nombre" name="nombre"class="form-control" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <button id="cancelar"type="button" class="btn btn-danger">Cancelar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="container" id="tabla">
        <div class="table-responsive col-8">
            <table class="table table-primary">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">MEDICAMENTO</th>
                        <th scope="col"> <button id="añadir" class="btn btn-success"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-capsule" viewBox="0 0 16 16">
                            <path d="M1.828 8.9 8.9 1.827a4 4 0 1 1 5.657 5.657l-7.07 7.071A4 4 0 1 1 1.827 8.9Zm9.128.771 2.893-2.893a3 3 0 1 0-4.243-4.242L6.713 5.429l4.243 4.242Z"/>
                            </svg> 
                            Añadir</button>
                        </th>
                    </tr>
                </thead>
                <tbody id="bodyMedicamentos">

                </tbody>
            </table>
        </div>
        
    </div>

    <form id="update" style="display:none !important;">
        <div class="container" id="updateCen">
            <div class="col-6">
                <div class="table mt-3 text-center" >
                    <table class="table table-primary">
                        <thead>
                            <tr>
                                <th scope="col">NOMBRE</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="bodyUpdateMedicamento" >
                            <tr class="">
                                <input type="hidden" name="updateId" id="updateId">
                                
                                <td scope="row">
                                    <input id="updateNombre" name="updateNombre" class="form-control" type="text" required>
                                </td>
                                <td>
                                    <button class="btn btn-success" type="submit">Guardar</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </form>


    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>
    <script src="../js/jquery-3.6.3.min.js"></script>
    <script src="../js/medicamentos.js"></script>
</body>
</html>