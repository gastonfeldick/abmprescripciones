<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Personas</title>
    
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="estilos/personas.css" rel="stylesheet">
    <link rel="icon" href="https://www.integrandosalud.com/src-v2/public/bootstrap/img/favicon/favicon.ico" type="image/x-icon">    
</head>
<body>

    <div id="nav">
        <button class="btn btn-success" id="menu"> MENU</button>
        <h1 id="titulo">PERSONAS</h1>       
    </div>
    
            
    <div class="container">

            <div class="col-6" id="form">
                <form action="" id="formularioPersonas" style="display:none !important;">
                    <div class="card">
                        <div class="card-header">
                            <h5>Nuevo paciente</h5>
                        </div>
                        <div class="card-body">
                            <div id="guardarPersona" class="alert alert-success text-center" style="display:none !important;">Guardado</div>
                            <div id="errorPersona" class="alert alert-danger text-center" style="display:none !important;">Error usuario ya existe</div>
                            <div class="row mt-1">
                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input id="nombre" name="nombre"class="form-control" type="text" required>
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="form-group">
                                    <label for="apellido">Apellido</label>
                                    <input id="apellido" name="apellido" class="form-control" type="text" required>
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="form-group">
                                    <label for="dni">Dni</label>
                                    <input name="dni" id="id" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="form-group">
                                    <label for="fechaNacimiento">Fecha de nacimiento</label>
                                    <input name="fechaNacimiento" id="fechaNacimiento" class="form-control" type="date">
                                </div>
                            </div>
                            
                        </div>
                        <div class="card-footer text-muted">
                            <button type="submit" class="btn btn-success">Guardar</button>
                            <button id="cancelar"type="button" class="btn btn-danger">Cancelar</button>
                        </div>
                    </div>
                </form>


            </div>

    </div>
    
    <div class="container" id="tabla" style="displapy:none !important;">
            
        <div class="col-12" id="tablaPersonas">
            <div class="table table-responsive mt-1 text-center">
                <table class="table table-primary">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">NOMBRE</th>
                            <th scope="col">Apellido</th>
                            <th scope="col">Dni</th>
                            <th scope="col">Fecha Nacimiento</th>
                            <th scope="col"> <button id="añadir" class="btn btn-success"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill-add" viewBox="0 0 16 16">
            <path d="M12.5 16a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7Zm.5-5v1h1a.5.5 0 0 1 0 1h-1v1a.5.5 0 0 1-1 0v-1h-1a.5.5 0 0 1 0-1h1v-1a.5.5 0 0 1 1 0Zm-2-6a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
            <path d="M2 13c0 1 1 1 1 1h5.256A4.493 4.493 0 0 1 8 12.5a4.49 4.49 0 0 1 1.544-3.393C9.077 9.038 8.564 9 8 9c-5 0-6 3-6 4Z"/>
            </svg>  
             Añadir</button></th>
                        </tr>
                    </thead>
                    <tbody id="bodyPersonas" >
                        
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <form action="" id="update" style="display:none !important;">
        <div class="container">
            <div class="col-12">
                <div class="table-responsive mt-3 text-center">
                    <table class="table table-primary">
                        <thead>
                            <tr>
                                <th scope="col">NOMBRE</th>
                                <th scope="col">Apellido</th>
                                <th scope="col">Dni</th>
                                <th scope="col">Fecha Nacimiento</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="bodyPersona" >
                            <tr class="">
                                <input type="hidden" name="updateId" id="updateId">
                                <td scope="row">
                                    <input id="updateNombre" name="updateNombre" class="form-control" type="text" required>
                                </td>
                                <td scope="row">
                                    <input id="updateApellido" name="updateApellido" class="form-control" type="text" required>
                                </td>
                                <td scope="row">
                                    <input id="updateDni" name="updateDni" class="form-control" type="text" required>
                                </td>
                                <td scope="row">
                                    <input id="updateFecha" name="updateFecha" class="form-control" type="date" required>
                                </td>
                                <td scope="row">
                                    <button class="btn btn-primary" type="submit">Guardar</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </form>
    
    

    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>
    <script src="../js/jquery-3.6.3.min.js"></script>
    <script src="../js/personas.js"></script>

</body>
</html>